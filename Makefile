.PHONY: all clean

CC ?=gcc
CXX ?= g++
all: clean a.out

a.out: main.c
	$(CXX) main.c
clean:
	rm -f a.out
