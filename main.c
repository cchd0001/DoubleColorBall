#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>


#ifndef __cplusplus
#ifndef bool
#define bool int
#define true (1)
#define false (0)
#endif
#endif

#define RED_REGION (33)
#define GREEN_REGION (16)
#define CHOOSE_REGION (7)
#define IS_RED(id) ( (id) < CHOOSE_REGION ? true : false )
#define LOCK_FILE ".lock"
#define TC_MAX (24)
#define MAX_WAIT_SECONDS (3600)

int GetRandomBallFromPoolWithoutPutBack(int *array,size_t n)
{
    assert(array!=NULL);
    int ret;
    do
    {
        int index = rand() % n;
        ret = array[index];
        if(ret != -1)
        {
            array[index] = -1;
            return ret ;
        }
    }while(true);
}

void InitIntArrayFrom1ToN(int * array, size_t n)
{
    assert(array!=NULL);
    for(size_t i = 0 ; i < n ; i++ )
    {
        array[i] = i+1;
    }
}


int cmp_int(const void * aa,const void * bb)
{
    assert(aa!=NULL && bb != NULL);
    int * a = (int*)aa;
    int * b = (int*)bb;

    return *a > *b ? 1 : *a==*b ? 0 : -1 ;
}

int PrevTime()
{
    FILE * fd = fopen(LOCK_FILE,"r");
    if(fd == NULL)
    {
        return 0;
    }
    char prev[TC_MAX];
    memset(prev,0,TC_MAX);
    if(fgets(prev,TC_MAX,fd))
    {
        return atoi(prev);
    }
    return 0;
}

bool IsNew()
{
    unsigned int curr = time(0);
    if(curr - PrevTime() < MAX_WAIT_SECONDS)
    {
        return false;
    }
    FILE *fd = fopen(LOCK_FILE,"w");
    fprintf(fd,"%u\n",curr);
    fclose(fd);
    return true;
}

int main()
{
    if(!IsNew())
    {
        printf("命运信则有不信则无\n");
        return 0;
    }
    int Green[GREEN_REGION];
    int Red[RED_REGION];
    int Choose[CHOOSE_REGION];
    int Result[CHOOSE_REGION];

    InitIntArrayFrom1ToN(Green,GREEN_REGION);
    InitIntArrayFrom1ToN(Red,RED_REGION);
    InitIntArrayFrom1ToN(Choose,CHOOSE_REGION);

    srand(time(0));

    for( int i = 0 ; i < CHOOSE_REGION ; i++ )
    {
        int index = GetRandomBallFromPoolWithoutPutBack(Choose,CHOOSE_REGION);
        if(IS_RED(index))
        {
            Result[index-1] = GetRandomBallFromPoolWithoutPutBack(Red,RED_REGION);
        }
        else
        {
            Result[index-1] = GetRandomBallFromPoolWithoutPutBack(Green,GREEN_REGION);

        }
    }
    qsort(Result,CHOOSE_REGION-1,sizeof(int),cmp_int);
    printf("信代码得永生\n");
    printf("+------------+\n");
    for( int i = 0 ; i < CHOOSE_REGION ; i++ )
    {
        printf("%s %d\n",IS_RED(i+1) ? "red" : "green" , Result[i]);
    }
    printf("+------------+\n");
    printf("Buy Now!\n");

    return 0;
}
